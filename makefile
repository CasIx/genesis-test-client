include .env
export $(shell sed 's/=.*//' .env)

.PHONY: run stop build login deploy compose test env

default: build

env: 
	printenv

run:
	docker-compose up ${SERVICE}

stop:
	docker stop ${SERVICE}

build:		
	docker build -t casoer/genesis-test:${SERVICE} .

login: 
	echo ${DOCKER_TOKEN} | docker login --username casoer --password-stdin

deploy:
	docker push casoer/genesis-test:${SERVICE}

compose:
	docker-compose up -d

test: compose
	npm run test