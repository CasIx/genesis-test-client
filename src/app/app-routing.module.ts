import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteComponent } from 'src/components/route/route.component';
import { RoutesComponent } from 'src/components/routes/routes.component';
import { TransportComponent } from 'src/components/transport/transport.component';
import { TransportsComponent } from 'src/components/transports/transports.component';

const routes: Routes = [{
  path: 'routes',
  component: RoutesComponent,
}, {
  path: 'transports',
  component: TransportsComponent,
}, {
  path: 'routes/:id',
  component: RouteComponent,
}, {
  path: 'transports/:id',
  component: TransportComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
