import { Id } from './repositories/crud.repository';

export * from './repositories'
export interface ContainsId {
    id: Id;
}