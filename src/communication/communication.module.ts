import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { TransportsRepository } from "./repositories/transports.repository";
import { RoutesRepository } from "./repositories/routes.repository";
import { CitiesRepository } from "./repositories/cities.repository";

@NgModule({
    imports: [HttpClientModule],
    providers: [
        TransportsRepository,
        RoutesRepository,
        CitiesRepository,
    ],
    // exports: [
    //     TransportsRepository,
    //     RoutesRepository,
    //     CitiesRepository,
    // ],
    
})
export class CommunicationModule {

}