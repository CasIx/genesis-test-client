import { Injectable } from "@angular/core";
import { IRoute } from "@genesis-test/core";
import { CrudRepository } from "./crud.repository";

@Injectable({
    providedIn: 'root'
})
export class RoutesRepository extends CrudRepository<IRoute> {
    protected baseUrl: string = '/api/routes';
    getItem(id, params) {
        return this.$http.get<IRoute>(this.createQuery(params, id));
    }
}