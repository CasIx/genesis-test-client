import { Injectable } from "@angular/core";
import { ITransport } from "@genesis-test/core";
import { CrudRepository } from "./crud.repository";

@Injectable({
    providedIn: 'root'
})
export class TransportsRepository extends CrudRepository<ITransport> {
    protected baseUrl: string = '/api/transports';
}