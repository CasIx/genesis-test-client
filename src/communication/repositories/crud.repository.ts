import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { RequestQueryBuilder } from "@nestjsx/crud-request";
import { Observable } from "rxjs";

RequestQueryBuilder.create();


interface PaginationResponse<T> {
    data: Array<T>;
    page: number;
    skip: number;
    limit: number;
    count: number;
    total: number;
}

interface CrudRequest extends RequestQueryBuilder {
    
}

export type Id = number | string;
export interface ContainsId {
    id: Id;
}

@Injectable()
export abstract class CrudRepository<T> {

    protected abstract readonly baseUrl: string;
    constructor(protected readonly $http: HttpClient) {}

    getItems(params?: CrudRequest): Observable<PaginationResponse<T>> {
        return this.$http.get<PaginationResponse<T>>(this.createQuery(params));
    }

    getItem(id: Id, params?: CrudRequest): Observable<T> {
        return this.$http.get<T>(this.createQuery(params, id));
    }

    createItem(item: T, params?: CrudRequest) {
        return this.$http.post<T>(this.createQuery(params), item);
    }   

    createItems(items: Array<T>, params?: CrudRequest): Observable<PaginationResponse<T>> {
        return this.$http.post<PaginationResponse<T>>(this.createQuery(params, 'bulk'), {
            bulk: items
        })
    }

    updateItem(id: Id, item: T, params?: CrudRequest): Observable<T> {
        return this.$http.patch<T>(this.createQuery(params, id), item);
    }

    deleteItem(id: Id, params?: CrudRequest): Observable<T> {
        return this.$http.delete<T>(this.createQuery(params, id))
    }

    protected createQuery(params?: CrudRequest, ...path: Array<any>): string {
        return [this.baseUrl, ...path].join('/') + '?' + (params?.query(false) ?? '');
    }

}