import { CrudRepository } from "./crud.repository";
import { ICity } from "@genesis-test/core"
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class CitiesRepository extends CrudRepository<ICity> {
    protected baseUrl: string = '/api/cities';
}