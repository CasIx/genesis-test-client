import { Component } from "@angular/core";
import { ItemsComponent } from "../base/items.component";
import { ITransport } from '@genesis-test/core';
import { TransportsRepository } from "src/communication";
import { ActivatedRoute } from "@angular/router";

@Component({
    templateUrl: './transports.component.html'
})
export class TransportsComponent extends ItemsComponent<ITransport> {
    constructor(
        repository: TransportsRepository,
        route: ActivatedRoute
    ) {
        super(repository, route);
    }

}
