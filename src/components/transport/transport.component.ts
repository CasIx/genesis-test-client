import { Component } from "@angular/core";
import { ItemComponent } from "../base/item.component";
import { ITransport, TransportStatus, TransportType } from "@genesis-test/core"
import { TransportsRepository } from "src/communication";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
    templateUrl: './transport.component.html'
})
export class TransportComponent extends ItemComponent<ITransport> {

    form: FormGroup;
    types: Array<string> = Object.values(TransportType);
    status: Array<string> = Object.values(TransportStatus);

    constructor(
        repository: TransportsRepository,
        route: ActivatedRoute,
        private readonly formBuilder: FormBuilder,
    ) {
        super(repository, route);
    }

    submit() {
        for (const i in this.form.controls) {
            this.form.controls[i].markAsDirty();
            this.form.controls[i].updateValueAndValidity();
        }
        this.save(this.form.value);
    }

    ngOnInit(): void {
        super.ngOnInit()
    }
    onItemLoad() {
        this.form = this.formBuilder.group({
            type: [this.item?.type ?? null, Validators.required],
            number: [this.item?.number ?? null, Validators.required],
            model: [this.item?.model ?? null, Validators.required],
            mileage: [this.item?.mileage ?? 0, Validators.required],
        });

    }


}