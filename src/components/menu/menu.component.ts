import { Component } from "@angular/core";


interface MenuItem {
    title: string;
    link: string;
}

@Component({
    templateUrl: './menu.component.html',
    selector: 'genesis-menu',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
    isCollapsed = false;
    items: Array<MenuItem> = [{
        title: 'Routes',
        link: '/routes'
    }, {
        title: 'Transports',
        link: '/transports'
    }];
}