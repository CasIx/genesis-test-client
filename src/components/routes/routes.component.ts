import { Component } from "@angular/core";
import { ItemsComponent } from "../base/items.component";
import { IRoute } from '@genesis-test/core';
import { RoutesRepository } from "src/communication";
import { ActivatedRoute } from "@angular/router";

@Component({
    templateUrl: './routes.component.html'
})
export class RoutesComponent extends ItemsComponent<IRoute> {
    constructor(repository: RoutesRepository, route: ActivatedRoute) {
        super(repository, route);
    }

    load() {
        return super.load({
            join: [{
                field: 'transport'
            }, {
                field: 'startCity'
            }, {
                field: 'finishCity'
            }]
        })
    }
}