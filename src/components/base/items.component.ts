import { Directive, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CreateQueryParams, RequestQueryBuilder } from "@nestjsx/crud-request";
import { ContainsId } from "src/communication";
import { CrudRepository } from "src/communication/repositories/crud.repository";

@Directive()
export  class ItemsComponent<T extends ContainsId> implements OnInit {

    public loading: boolean = true;
    public items: Array<T>;
    public page: number = 1;
    public count = 0;

    handleError = e => {
        this.loading = false;
        alert(e.error.message)
    }

    constructor(
        protected readonly repository: CrudRepository<T>,
        protected readonly route: ActivatedRoute,
    ) { }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.page = Number.parseInt(params['page'] ?? "1")
            this.load();
        })
    }

    load(params: CreateQueryParams = {}) {
        this.loading = true;
        this.repository.getItems(RequestQueryBuilder.create(params).setPage(this.page))
            .subscribe((items) => {
                this.items = items.data;
                this.count = items.total
                this.loading = false;
            }, this.handleError)
    }

    onPageChange(page: number) {
        this.page = page;
        this.load();
    }

    deleteItem(item: T) {
        
        const c = confirm('You really want to delete this item?')
        if (c) {
            this.loading = true;
            this.repository.deleteItem(item.id)
                .subscribe(() => {
                    this.load()
                },this.handleError)
        }
    }

    

}