import { Directive, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CreateQueryParams, RequestQueryBuilder } from "@nestjsx/crud-request";
import { Observable } from "rxjs";
import { ContainsId, CrudRepository, Id } from "src/communication/repositories/crud.repository";

@Directive()
export class ItemComponent<T extends ContainsId> implements OnInit {

    public loading: boolean = true;
    public item: Partial<T>;
    public needCreate: boolean = false;
    protected params: CreateQueryParams = {}


    constructor(
        protected readonly repository: CrudRepository<T>,
        private readonly route: ActivatedRoute
    ) { }
    
    handleError = e => {
        this.loading = false;
        alert(e.error.message)
    }

    ngOnInit(): void {
        this.route.params.subscribe((params) => {
            if (params['id'] === 'create') {
                this.needCreate = true;
                this.item = {};
                this.loading = false;
                this.onItemLoad()
            } else {
                this.loadById(params['id'], RequestQueryBuilder.create(this.params))
                    .subscribe((response) => {
                        this.item = response;
                        this.loading = false;
                        this.onItemLoad();
                    }, this.handleError)
            }
        })
    }

    loadById(id: Id, params?: any): Observable<T> {
        return this.repository.getItem(id, params)
    }

    onItemLoad() { }

    update(item: T) {
        this.loading = true;
        this.repository.updateItem(this.item.id, item, RequestQueryBuilder.create(this.params))
            .subscribe((response) => {
                this.item = response;   
                this.loading = false;
                this.needCreate = false;
            },this.handleError)
    }

    create(item: Exclude<T, 'id'>) {
        this.loading = true;
        this.repository.createItem(item, RequestQueryBuilder.create(this.params))
            .subscribe((response) => {
                this.item = response;
                this.loading = false;
                this.needCreate = false;
            }, this.handleError)
    }

    save(item: any) {
        if(this.needCreate) {
            return this.create(item);
        }
        return this.update(item);
    }

}