import { Component, Input, Output } from "@angular/core";
import { CreateQueryParams, QueryFilter, QueryFilterArr, RequestQueryBuilder } from "@nestjsx/crud-request";
import { CrudRepository } from "src/communication/repositories/crud.repository";

@Component({
    selector: 'select-items',
    templateUrl: './select-item.component.html',
})
export class SelectItemComponent {


    @Input()
    onItemSelected: Function;

    @Input('item')
    _item: any;

    @Input()
    view: (item: any) => string

    @Input()
    set filters(filters: Array<QueryFilter>) {
        if (RequestQueryBuilder.create({filter: filters}).query() != RequestQueryBuilder.create({ filter: this._filters}).query()) {
            this._filters = filters;
            this.load();
        }
    }
    get filters() {
        return this._filters;
    }
    @Input()
    searchField: string;
    @Input()
    repository: CrudRepository<any>;
    items: Array<any>;
    search: string = null;
    loading = true;

    private _filters: any = [];

    set item(item) {
        this._item = item;
        this.onItemSelected(item);
    }

    get item() {
        return this._item;
    }

    ngOnInit() {
        this.repository.getItems()
    }

    load() {
        this.loading = true;
        let filters = this.filters;
        if (this.search) {
            filters = [...filters, {
                field: this.searchField,
                operator: '$contL',
                value: this.search,
            }]
        }
        this.repository.getItems(RequestQueryBuilder.create({
            filter: [...this.filters]
        }))
            .subscribe((items) => {
                this.loading = false;
                this.items = items.data;
            }, () => {
                this.items = [];
                this.loading = false;
            })
    }

    onSearch(search: string) {
        this.search = search;
        this.load()
    }

}