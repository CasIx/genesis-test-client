import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Currency, ICity, IRoute, ITransport, RouteStatus, TransportType } from "@genesis-test/core";
import { CreateQueryParams } from "@nestjsx/crud-request";
import { CitiesRepository, RoutesRepository, TransportsRepository } from "src/communication";
import { ItemComponent } from "../base/item.component";


@Component({
    templateUrl: './route.component.html'
})
export class RouteComponent extends ItemComponent<IRoute> {

    setTransport: boolean = false;

    transportTypes = Object.values(TransportType);
    currencies = Object.values(Currency);
    params: CreateQueryParams = {
        join: [{
            field: 'transport'
        }, {
            field: 'startCity'
        }, {
            field: 'finishCity'
        }]
    }

    get transportFilters() {
        if (this.item.transportType) {
            return [{
                field: 'type',
                operator: '$eq',
                value: this.item.transportType,
            }]
        }
        return []
    }

    constructor(
        repository: RoutesRepository,
        route: ActivatedRoute,
        public readonly citiesRepository: CitiesRepository,
        public readonly transportsRepository: TransportsRepository,
    ) {
        super(repository, route);
    }

    cityView(city: ICity) {
        return city.name;
    }

    transportView(transport: ITransport) {
        return transport.model;
    }


    onStartCitySelected = (city: ICity) => {
        this.item.startCity = city;
    }

    onFinishCitySelected = (city: ICity) => {
        this.item.finishCity = city;
    }

    onTransportSelected = (transport: ITransport) => {
        this.item.transport = transport;
        this.item.transportType = transport.type;
    }

    start() {
        this.item.status = RouteStatus.InProgress;
        this.save(this.item);
    }

    finish() {
        this.item.status = RouteStatus.Finished;
        this.save(this.item)
    }

    save(item) {
        super.save(item)
    }


    //lat/lmg 1 - startCity
    //lat/lng 2 - finishCity
    get distance(): number | undefined {
        const R = 6371e3; // metres
        const f1 = this.item.startCity.lat * Math.PI / 180; // φ, λ in radians
        const f2 = this.item.finishCity.lat * Math.PI / 180;
        const d1 = (this.item.finishCity.lat - this.item.startCity.lat) * Math.PI / 180;
        const d2 = (this.item.finishCity.lng - this.item.startCity.lng) * Math.PI / 180;

        const a = Math.sin(d1 / 2) * Math.sin(d1 / 2) +
            Math.cos(f1) * Math.cos(f2) *
            Math.sin(d2 / 2) * Math.sin(d2 / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        const d = R * c; // in metres
        return Math.round((d * 0.001 + Number.EPSILON) * 100) / 100
    }

    changeTransport() {
        this.setTransport = !this.setTransport;
    }
}